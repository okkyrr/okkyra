<?php

use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiswaController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});


Route::get('/login/admin', function () {
    return view('Admin.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('ceklevel:siswa,admin,pembimbing');
Route::get('/adminHome', [App\Http\Controllers\AdminController::class, 'index'])->name('homeAdmin')->middleware('ceklevel:admin');

Route::get('/dashboard/siswa', [App\Http\Controllers\SiswaController::class, 'dashboard'])->middleware('ceklevel:siswa', 'auth');
Route::get('/dashboard/pembimbing', [App\Http\Controllers\PembimbingController::class, 'dashboard'])->middleware('ceklevel:pembimbing', 'auth');
Route::get('/dashboard/siswa/pengajuan_pkl', [App\Http\Controllers\SiswaController::class, 'pengajuan_pkl'])->middleware('ceklevel:siswa', 'auth');
Route::get('/dashboard/monitoring', [App\Http\Controllers\PembimbingController::class, 'monitoring'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/dashboard/jurnalSiswa', [App\Http\Controllers\PembimbingController::class, 'jurnalSiswa'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/dashboard/Cek_kegiatan/{id}', [App\Http\Controllers\PembimbingController::class, 'cekKegiatan'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/dashboard/Pengajuan_PKL', [App\Http\Controllers\PembimbingController::class, 'Pengajuan_PKL'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/updateKeteranganJurnal', [App\Http\Controllers\PembimbingController::class, 'updateKeteranganJurnal'])->middleware('ceklevel:admin,pembimbing', 'auth');

Route::get('/editProfile', [App\Http\Controllers\SiswaController::class, 'index'])->name('editProfile')->middleware('ceklevel:siswa', 'auth');
Route::post('/editProfile', [App\Http\Controllers\SiswaController::class, 'update'])->name('editProfile');

Route::get('/siswaTables', [App\Http\Controllers\SiswaController::class, 'showAllSiswa'])->name('showAllSiswa')->middleware('ceklevel:admin,pembimbing,siswa', 'auth');
Route::get('/detail/{id}', [App\Http\Controllers\SiswaController::class, 'detailSiswa'])->name('detailSiswa')->middleware('ceklevel:admin,pembimbing,siswa', 'auth');

Route::get('/laporan', [App\Http\Controllers\SiswaController::class, 'laporan'])->name('laporan')->middleware('auth');
Route::post('/createLaporan', [App\Http\Controllers\SiswaController::class, 'createLaporan'])->name('createLaporan')->middleware('auth');
Route::post('/updateLaporan', [App\Http\Controllers\SiswaController::class, 'updateLaporan'])->name('updateLaporan')->middleware('auth');
Route::post('/validasiLaporan', [App\Http\Controllers\PembimbingController::class, 'validasiLaporan'])->name('validasiLaporan')->middleware('auth');

Route::get('/pembimbingTables', [App\Http\Controllers\PembimbingController::class, 'showAllPembimbing'])->name('showAllPembimbing')->middleware('ceklevel:admin,pembimbing,siswa', 'auth');
Route::get('/createPembimbing', [App\Http\Controllers\PembimbingController::class, 'createPembimbing'])->name('createPembimbing')->middleware('ceklevel:admin', 'auth');
Route::post('/createPembimbing', [App\Http\Controllers\PembimbingController::class, 'insertPembimbing'])->name('createPembimbing');
Route::get('/detailPembimbing/{id}', [App\Http\Controllers\PembimbingController::class, 'detailPembimbing'])->middleware('ceklevel:admin,pembimbing,siswa', 'auth');

Route::get('/cekLaporan', [App\Http\Controllers\PembimbingController::class, 'cekLaporan'])->middleware('ceklevel:admin,pembimbing,siswa', 'auth');

Route::get('/editSiswa/{id}', [App\Http\Controllers\AdminController::class, 'formupdate'])->name('formeditProfile')->middleware('ceklevel:admin', 'auth');
Route::post('/editSiswa', [App\Http\Controllers\AdminController::class, 'update'])->name('editSiswa');
Route::get('/setting', [App\Http\Controllers\AdminController::class, 'setting']);
Route::post('/insertJurusan', [App\Http\Controllers\AdminController::class, 'insertJurusan']);
Route::post('/updateJurusan', [App\Http\Controllers\AdminController::class, 'updateJurusan']);
Route::get('/deleteJurusan/{id}', [App\Http\Controllers\AdminController::class, 'deleteJurusan']);
Route::get('/deletePerusahaan/{id}', [App\Http\Controllers\AdminController::class, 'deletePerusahaan']);

Route::get('/createAccount', [App\Http\Controllers\AdminController::class, 'createAccount'])->name('createAccount')->middleware('ceklevel:admin', 'auth');
Route::post('/createAccount', [App\Http\Controllers\AdminController::class, 'insert'])->name('createAccount')->middleware('ceklevel:admin', 'auth');
Route::get('/deleteSiswa/{id}', [App\Http\Controllers\AdminController::class, 'delete'])->name('createAccount')->middleware('ceklevel:admin', 'auth');

Route::get('/lanjut', [App\Http\Controllers\SiswaController::class, 'nextProgress'])->middleware('auth');
Route::post('/upload/pengantar', [App\Http\Controllers\SiswaController::class, 'upload_pengantar'])->middleware('auth');
Route::get('/download/{file_name}', [App\Http\Controllers\SiswaController::class, 'download'])->middleware('auth');

Route::post('/download', [App\Http\Controllers\PembimbingController::class, 'download'])->middleware('auth');
Route::get('/validasiPengantar/{id}', [App\Http\Controllers\PembimbingController::class, 'validasiPengantar'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::get('/konfirmasiPengajuan/{id}', [App\Http\Controllers\PembimbingController::class, 'konfirmasiPengajuan'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/prosesValidasiPengantar', [App\Http\Controllers\PembimbingController::class, 'prosesValidasiPengantar'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/addPembimbing', [App\Http\Controllers\PembimbingController::class, 'addPembimbing'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/konfirmasiPengajuan', [App\Http\Controllers\PembimbingController::class, 'proseskonfirmasiPengajuan'])->middleware('auth');

Route::get('/perusahaanTables', [App\Http\Controllers\PerusahaanController::class, 'index'])->middleware('ceklevel:admin,pembimbing', 'auth');
Route::post('/createPerusahaan', [App\Http\Controllers\AdminController::class, 'insertPerusahaan'])->middleware('ceklevel:admin', 'auth');
Route::post('/insertTempatPkl', [App\Http\Controllers\SiswaController::class, 'insertTempatPkl'])->middleware('auth');

Route::post('/gantiStatus', [App\Http\Controllers\SiswaController::class, 'gantiStatus']);

Route::get('/jurnal', [App\Http\Controllers\JurnalController::class, 'index'])->middleware('auth');
Route::post('/jurnal/tambah', [App\Http\Controllers\JurnalController::class, 'tambah'])->middleware('auth');
Route::get('/jurnal/edit/{id}', [App\Http\Controllers\JurnalController::class, 'edit'])->middleware('auth');
Route::post('/jurnal/update', [App\Http\Controllers\JurnalController::class, 'update'])->middleware('auth');

Route::get('/table', [App\Http\Controllers\HomeController::class, 'ShowTables'])->middleware('auth');
Route::get('/gantiPassword', [App\Http\Controllers\SiswaController::class, 'gantiPassword'])->middleware('auth');
Route::patch('/gantiPassword', [App\Http\Controllers\SiswaController::class, 'updatePassword'])->name('user.password.update')->middleware('auth');
// Route::patch('password', 'PasswordController@update')->name('user.password.update');
// Route::get('/jurnal/edit/{id}', [App\Http\Controllers\JurnalController::class, 'edit']);
