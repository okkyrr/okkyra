<?php

namespace App\Http\Controllers;

use App\Models\guru;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\pembimbing;
use App\Models\perusahaan;
use App\Models\laporan;
use Illuminate\Support\Facades\Hash;
use app\Http\Requests\UpdatePasswordRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class SiswaController extends Controller
{
    public function dashboard()
    {
        $data = ['perusahaan' => perusahaan::all()->sortBy('name')];
        return view('Siswa.dashboard', $data);
    }
    public function pengajuan_pkl()
    {
        $data = ['perusahaan' => perusahaan::all()->sortBy('name')];
        return view('Siswa.Home', $data);
    }
    public function index()
    {
        $data = ['data' => pembimbing::all()->sortBy('name')];
        return view('Siswa.formEditProfile', $data);
    }
    public function update(Request $request)
    {
        DB::table('users')->where('id', $request->id)->update([
            'name' => $request->name,
            'nis' => $request->nis,
            'kelas' => $request->kelas,
            'jurusan_id' => $request->jurusan,
            'username' => $request->username,
            'email' => $request->email,
        ]);
        return redirect('/home');
    }
    public function showAllSiswa()
    {
        $data = ['data' => User::all()->where('level', "siswa")->sortBy('name')];
        return view('Siswa.siswaTables', $data);
    }
    public function detailSiswa($id)
    {
        $data = ['data' => User::where('id', $id)->first()];
        return view('Siswa.detailSiswa', $data);
    }
    public function nextProgress()
    {
        if (Auth()->user()->status == null) {
            return view('Siswa.pengajuanPengantar');
        } elseif (Auth()->user()->status == 'Menunggu Proses Validasi Surat Pengantar') {
            $data = ['data' => user::all()->where('id', Auth()->user()->id)];
            return view('Siswa.validasiPengantar', $data);
        }
    }
    public function upload_pengantar(Request $request)
    {

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload, $nama_file);

        DB::table('users')->where('id', $request->id)->update([
            'pengantar_pkl' => $nama_file,
            'status' => $request->status
        ]);

        return redirect()->back();
    }
    public function download($file_name)
    {
        $file_path = public_path('data_file/' . $file_name);
        return response()->download($file_path);
    }
    public function insertTempatPkl(Request $request)
    {
        DB::table('users')->where('id', $request->id)->update([
            'perusahaan_id' => $request->perusahaan_id,
            'status' => $request->status
        ]);
        return Redirect('/dashboard/siswa/pengajuan_pkl');
    }
    public function gantiStatus(Request $request)
    {
        DB::table('users')->where('id', $request->id)->update([
            'status' => $request->status
        ]);
        return Redirect('/home');
    }
    public function gantiPassword()
    {
        return view('Auth.passwords.updatePassword');
    }

    /**
     * @param UpdatePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(Request $request)
    {


        $request->user()->update([
            'password' => bcrypt($request->password)
        ]);

        return redirect('/home');
    }
    public function laporan()
    {
        $data = laporan::all()->where('id', 1);
        return view('Siswa.Laporan', [
            'data' => $data
        ]);
    }
    public function createLaporan(Request $request)
    {
        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file_laporan');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload, $nama_file);

        DB::table('users')->where('id', $request->id)->update([
            'laporan_id' => $request->id
        ]);

        DB::table('laporans')->INSERT([
            'id' => $request->id,
            'keterangan' => $request->keterangan,
            'file_laporan' => $nama_file,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect()->back();
    }
    public function updateLaporan(Request $request)
    {
        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file_laporan');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload, $nama_file);

        DB::table('laporans')->where('id', $request->id)->update([
            'file_laporan' => $nama_file,
            'keterangan' => $request->keterangan,
            'updated_at' => now()
        ]);

        return redirect()->back();
    }
}